/**
 * Created by dfinlay-air on 09/10/16.
 */
var Bear     = require('./models/bear');

module.exports = function(app) {



    // create a bear (accessed at POST http://localhost:8080/api/bears)
    app.post('/bears',function(req, res) {

        var bear = new Bear();      // create a new instance of the Bear model
        bear.name = req.body.name;  // set the bears name (comes from the request)

        // save the bear and check for errors
        bear.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'Bear created!' });
        });

    });


    app.get('/bears',function(req, res) {
        Bear.find(function(err, bears) {
            if (err)
                res.send(err);

            res.json(bears);
        });
    });




}