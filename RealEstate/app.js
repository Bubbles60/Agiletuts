// server.js

// BASE SETUP
// =============================================================================

var mongoose   = require('mongoose');

// call the packages we need
var express    = require('express');        // call express
var app       = express();                 // define our app using express
var bodyParser = require('body-parser');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
//var appr = express.Router();              // get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
//router.get('/', function(req, res) {
//    res.json({ message: 'hooray! welcome to our api!' });   
//});

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
// ROUTES FOR OUR API

mongoose.connect('mongodb://bears:bears@jello.modulusmongo.net:27017/o7natIpe'); // connect to our database

var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    // we're connected!
    console.log('We are connected to db .');
});

var router = express.Router();
/*
// =============================================================================
             // get an instance of the express Router

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api !' });   
});

// more routes for our API will happen here
// on routes that end in /bears
// ----------------------------------------------------


    // create a bear (accessed at POST http://localhost:8080/api/bears)
router.post('/bears',function(req, res) {
        
        var bear = new Bear();      // create a new instance of the Bear model
        bear.name = req.body.name;  // set the bears name (comes from the request)

        // save the bear and check for errors
        bear.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'Bear created!' });
        });
        
    });


router.get('/bears',function(req, res) {
    Bear.find(function(err, bears) {
        if (err)
            res.send(err);

        res.json(bears);
    });
});




// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
//app.use('/api', app);

//==============================

*/
// routes ======================================================================
require('./app/routes.js')(app);
app.all('/api',app)
// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);